#include <string>
extern "C" {
    #include <lua.h>
    #include <lualib.h>
    #include <lauxlib.h>
    #include <wtypes.h>
	#include <mysql.h>
	
}

#pragma comment (lib, "lua5.1.lib")
#pragma comment (lib, "libmariadb.lib")

using namespace std;

extern "C" {
    lua_State* luaState;
	
	int SqlQueryti(string& ServerIp ,int port ,string& name,string& pass, string& BdName, string& query, string& result);

	static int LuaSqlQueryti(lua_State *L)
	{
		string ServerIp = lua_tostring(L, 1);
		int port = lua_tointeger(L,2);
		string name = lua_tostring(L, 3);
		string pass = lua_tostring(L, 4);
		string BdName = lua_tostring(L, 5);
		string query = lua_tostring(L, 6);

		string result;
		
		int r= SqlQueryti(ServerIp,port,name,pass,BdName,query,result);
		lua_pushinteger(L,r);
		lua_pushstring(L,result.c_str());
		return 2;
	}
	
	int SqlQueryti(string& ServerIp ,int port ,string& name,string& pass, string& BdName, string& query, string& result)
	{
	 MYSQL *conn;
	 MYSQL_RES *res;
	 MYSQL_ROW row;
	 int i = 0;
	 bool IsConected = false;
	 
	 // Connection handle
	 conn = mysql_init(NULL);
	 result = "";
	 if(conn == NULL)
		{
			// If the handle is not obtained - print an error message
			result = "Error: can't create MySQL-descriptor \n";
			return 1;
		}
	 // Connect to the server
	 if(!mysql_real_connect(conn, ServerIp.c_str(), name.c_str(), pass.c_str(), BdName.c_str(), port, NULL, 0))
		{
			// If you can't connect to the server
			// Database print an error message
			result = "Error: can't connect to database \n";
			return 2;
		}
	else
		{
			// If the connection is successful
			IsConected = true;
			mysql_set_character_set(conn, "utf8");
			
		}
	  
		string tempSrt ="";
		if(IsConected)
		{
			mysql_query(conn, query.c_str()); //Making a request

			if (res = mysql_store_result(conn))
				{
					while(row = mysql_fetch_row(res)) 
					{
						for (i=0 ; i < int(mysql_num_fields(res)); i++)
						{
							result += row[i]; 
							result += " ";
						}
					}
				 mysql_close(conn);
				 return 0;
				}
			else 
			{
			 result =(stderr, "\n", mysql_error(conn));
			 mysql_close(conn);
			 return 3;
			}
		}
	return 0;
	}

    static int GetPID(lua_State *L)
    {
        DWORD pid = GetCurrentProcessId();
        lua_pushnumber(L, pid);
        return 1;
    }
    
    static const luaL_Reg libFuncs [] = {        
        {"SqlQueryti", LuaSqlQueryti},        
        {"GetPID", GetPID},
        {NULL, NULL}
    };

    int __declspec(dllexport) luaopen_LuaMysql(lua_State *L) {        
        luaL_register(L, "LuaMysql", libFuncs);        
        luaState = L;
        return 1;
    }
}