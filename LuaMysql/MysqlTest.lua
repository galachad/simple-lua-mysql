LuaMysql={}
assert(package.loadlib( [[.\LuaMysql.dll]], 'luaopen_LuaMysql'))()    


--example
--status,result=LuaMysql.SqlQueryti("ServerIp",5009,"username","password","BdName","query")
function LuaMysql.Test()
	
	status,result=LuaMysql.SqlQueryti("127.0.0.1",3306,"user","YXLc4ecwaCJv2VAC","BdTest","SELECT * FROM  `test` ")
	if status==0 or status==3 then
		print(result) --it's result of query
	else
		--error
		--status==1 can't create MySQL-descriptor
		--status==2 can't connect to database 
	end
end

LuaMysql.Test()