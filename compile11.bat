@echo off

@: Environment
@set PATH=C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE;%PATH%
@set PATH=C:\Program Files (x86)\Microsoft Visual Studio 11.0\VC\BIN;%PATH%
@set LIB=C:\Program Files (x86)\Microsoft Visual Studio 11.0\VC\LIB;%LIB%
@set LIB=C:\Program Files (x86)\Microsoft SDKs\Windows\v7.1A\Lib;%LIB%
@set LIB=C:\Program Files (x86)\Lua\5.1\lib;%LIB%
@set LIB=.\mariadb_client\win32\mariadbclient\lib;%LIB%
@set INCLUDE=C:\Program Files (x86)\Microsoft Visual Studio 11.0\VC\include;%INCLUDE%
@set INCLUDE=C:\Program Files (x86)\Lua\5.1\include;%INCLUDE%
@set INCLUDE=.\mariadb_client\win32\mariadbclient\include;%INCLUDE%
@set INCLUDE=C:\Program Files (x86)\Microsoft SDKs\Windows\v7.1A\Include;%INCLUDE%




@: Server
@del "./LuaMysql.dll"
cl.exe /nologo /MT /W3 /O2 /Gd /Fo"./LuaMysql.obj" /FD /c "./LuaMysql.cpp"
link.exe /nologo /dll /incremental:no /machine:I386 "./LuaMysql.obj" /out:"./LuaMysql.dll"

@: Delete unnecessary stuff
@del "./LuaMysql.obj"
@del "./LuaMysql.exp"
@del "./LuaMysql.lib"
@del "./vc110.idb"

@pause